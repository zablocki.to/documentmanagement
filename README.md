# To set up project locally:
### DocumentManagement (Spring boot backend with Spring ElasticSeaarch):
1. Configure ElasticSearch:

a) In /src/main/resources/application.properties configure ElasticSearch:
   spring.elasticsearch.rest.uris=...
   spring.elasticsearch.rest.username=...
   spring.elasticsearch.rest.password=...
   spring.elasticsearch.rest.sniff=...
*You can use docker ElasticSearch image for local run:

b) docker pull elasticsearch:8.10.2

c) docker run -d --name elasticsearch -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:8.10.2

d) Default User: elastic

e) To get password run: docker exec -it <container_id> /usr/share/elasticsearch/bin/elasticsearch-reset-password -u elastic

To see logs: docker logs elasticsearch
To prove that elasticsearch docker Image works: 
    https://localhost:9200/_cluster/health
or
   curl -k -u <user>:<password> https://localhost:9200
2. Configure Google Drive (free) or implement own file storage service 
with cloud provider of your choice.

   To configure Google Drive and get required credentials:
   
   a) Create google account.
   
   b) Visit the Google API Console: https://console.cloud.google.com
   
   c) Create a new project or select an existing one.
   
   d) Go to "Library" and search for "Google Drive API", then activate it.
   
   e) Navigate to "Credentials" and choose "Create Credentials", then "OAuth Client ID".
   
   f) Configure the consent screen with necessary information.
   
   g) Select the application type, provide required details, and confirm.
   
   h) Download the credentials.json file provided after this process.

   i) Copy this file into resources folder.
   
   j) In /src/main/resources/application.properties configure google drive if used:

   Storage configuration:
storage.bean.name=googleDriveStorageService # name of the bean with FileStorageService implementation

   Google drive files storage configuration:
google.drive.certificate.path=...  # path to credentials in resources
google.drive.application.name=... # application name set in google drive
google.drive.tokens.directory.name=... # name of the directory to keep tokens for google drive access
    
   k) for first run of the application log in into google account set in google drive will be required
3. Start service backend with class DocumentManagementApplication.

### DocumentManagementUI (Angular Frontend):
1. Start UI ng serve
2. Go to if not set differently: http://localhost:4200/