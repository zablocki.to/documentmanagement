package com.documentmanagement.configuration;

import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchConfiguration;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

@Configuration
public class ElasticSearchConfiguration extends ElasticsearchConfiguration {
    //Docks: https://docs.spring.io/spring-data/elasticsearch/docs/current/reference/html/#elasticsearch.clients
    @Value("${spring.elasticsearch.rest.uris}")
    private String uri;
    @Value("${spring.elasticsearch.rest.username}")
    private String username;
    @Value("${spring.elasticsearch.rest.password}")
    private String password;

    @Override
    public ClientConfiguration clientConfiguration() {
        return ClientConfiguration.builder()
                .connectedTo(uri)
                //.usingSsl(sslContext())
                .withBasicAuth(username, password)
                .build();
    }

    private SSLContext sslContext(){ //TODO replaces with real ssl dealing
        try {
            return SSLContextBuilder
                    .create()
                    .loadTrustMaterial(null, (x509Certificates, s) -> true)
                    //.loadTrustMaterial(truststore, truststorePassword.toCharArray())
                    .build();
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
            throw new RuntimeException(e);
        }
    }

}