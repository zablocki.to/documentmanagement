package com.documentmanagement.controller;

import com.documentmanagement.controller.result.CreateResultService;
import com.documentmanagement.controller.result.PageResultObject;
import com.documentmanagement.model.ClientRepository;
import com.documentmanagement.model.entity.Client;
import com.documentmanagement.service.ClientService;
import com.documentmanagement.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/clients")
public class ClientController {
    private final ClientRepository repository;
    private final ClientService clientService;
    private final DocumentService documentService;
    private final CreateResultService<Client> createResultService;

    @Autowired
    public ClientController(ClientRepository repository,
                            ClientService clientService,
                            DocumentService documentService,
                            CreateResultService<Client> createResultService) {
        this.repository = repository;
        this.clientService = clientService;
        this.documentService = documentService;
        this.createResultService = createResultService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Client createOrUpdate(@RequestPart(value = "photo", required = false) MultipartFile photo,
                                 @RequestPart("client") String clientJson) {
        return saveClientWithPhoto(clientJson, photo);
    }

    @GetMapping("/{id}")
    public Optional<Client> findById(@PathVariable String id) {
        return repository.findById(id);
    }

    @GetMapping
    public PageResultObject<Client> findCurrentPage(@RequestParam("pageNum") int pageNum,
                                                    @RequestParam("pageSize") int pageSize,
                                                    @RequestParam(value = "nameFilter", defaultValue = "") String nameFilter) {
        PageRequest paging = PageRequest.of(pageNum, pageSize);
        Page<Client> currentPage = repository.findByFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(nameFilter, nameFilter, paging);
        return createResultService.cretePagedResult(currentPage);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Client update(@PathVariable String id, @RequestBody Client client){
        client.setId(id);
        return repository.save(client);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String id){
        documentService.removeDocumentsForEntity(id);
        repository.deleteById(id);
    }

    private Client saveClientWithPhoto(String clientJson, MultipartFile photo){
        return clientService.saveClientWithPhoto(clientJson, photo);
    }
}
