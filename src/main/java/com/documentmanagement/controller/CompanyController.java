package com.documentmanagement.controller;

import com.documentmanagement.model.ClientRepository;
import com.documentmanagement.model.CompanyRepository;
import com.documentmanagement.model.entity.Client;
import com.documentmanagement.model.entity.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

@CrossOrigin//(origins = "http://localhost:4200")
@RestController
@RequestMapping("/companies")
public class CompanyController {
    private final CompanyRepository repository;

    @Autowired
    public CompanyController(CompanyRepository repository) {
        this.repository = repository;
    }

    @PostMapping
    public Company create(@RequestBody Company company) {
        return repository.save(company);
    }

    @GetMapping("/{id}")
    public Optional<Company> findById(@PathVariable String id) {
        return repository.findById(id);
    }

    @GetMapping
    public List<Company> findAll() {
        return StreamSupport.stream(repository.findAll().spliterator(), false).toList();
    }

    @PutMapping("/{id}")
    public Company update(@PathVariable String id, @RequestBody Company company){
        company.setId(id);
        return repository.save(company);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable String id){
        repository.deleteById(id);
    }
}
