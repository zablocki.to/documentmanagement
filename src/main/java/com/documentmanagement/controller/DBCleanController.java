package com.documentmanagement.controller;

import com.documentmanagement.service.ClientService;
import com.documentmanagement.service.DocumentService;
import com.documentmanagement.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@RequestMapping("/clean")
public class DBCleanController {
    private ClientService clientService;
    private EmployeeService employeeService;
    private DocumentService documentService;

    @Autowired
    public DBCleanController(ClientService clientService,
                             EmployeeService employeeService,
                             DocumentService documentService) {
        this.clientService = clientService;
        this.employeeService = employeeService;
        this.documentService = documentService;
    }

    @DeleteMapping("/clients")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void cleanClients(){
        clientService.removeAllClients();
    }

    @DeleteMapping("/employees")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void cleanEmployees(){
        employeeService.removeAllEmployees();
    }

    @DeleteMapping("/documents")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void cleanDocuments(){
        documentService.removeAllDocuments();
    }
}
