package com.documentmanagement.controller;

import com.documentmanagement.model.DocumentRepository;
import com.documentmanagement.model.entity.cons.AssignedEntity;
import com.documentmanagement.model.entity.DocumentEnt;
import com.documentmanagement.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin//(origins = "http://localhost:4200")
@RestController
@RequestMapping("document")
public class DocumentController {
    private DocumentRepository documentRepository;
    private DocumentService documentService;

    @Autowired
    public DocumentController(DocumentRepository documentRepository,
                              DocumentService documentService) {
        this.documentRepository = documentRepository;
        this.documentService = documentService;
    }

    @GetMapping("{documentName}/{assignedEntity}/{assignedEntityId}")
    public Optional<DocumentEnt> getDocument(@PathVariable String documentName,
                                                     @PathVariable String assignedEntity,
                                                     @PathVariable String assignedEntityId){
        return documentRepository.findById(documentName + assignedEntity + assignedEntityId);
    }

    @GetMapping("document-names/{assignedEntity}/{assignedEntityId}")
    public Set<String> getDocumentNames(@PathVariable AssignedEntity assignedEntity,
                                        @PathVariable String assignedEntityId){
        return documentRepository.findByAssignedEntityAndAssignedEntityId(assignedEntity, assignedEntityId).stream()
                .map(DocumentEnt::getName)
                .collect(Collectors.toSet());
    }

    @PostMapping()
    public DocumentEnt saveDocument(@RequestPart("file") MultipartFile file,
                                    @RequestPart("document") String documentJson) throws IOException {
        String filePath = documentService.saveFile(file);
        return documentService.saveDocumentMetadata(documentJson, filePath);
    }

    @DeleteMapping("{documentName}/{assignedEntity}/{assignedEntityId}")
    public ResponseEntity<DocumentEnt> deleteDocument(@PathVariable String documentName,
                                                      @PathVariable String assignedEntity,
                                                      @PathVariable String assignedEntityId){
        Optional<DocumentEnt> docOpt = documentRepository.findById(documentName + assignedEntity + assignedEntityId);
        return docOpt.flatMap(doc -> documentService.removeFile(doc))
                .map(doc -> new ResponseEntity<>(documentService.removeDocumentMetadata(doc), HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
