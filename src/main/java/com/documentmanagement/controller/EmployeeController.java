package com.documentmanagement.controller;

import com.documentmanagement.controller.result.CreateResultService;
import com.documentmanagement.controller.result.PageResultObject;
import com.documentmanagement.model.EmployeeRepository;
import com.documentmanagement.model.entity.Client;
import com.documentmanagement.model.entity.Employee;
import com.documentmanagement.service.DocumentService;
import com.documentmanagement.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

@CrossOrigin//(origins = "http://localhost:4200")
@RestController
@RequestMapping("/employees")
public class EmployeeController {
    private final EmployeeRepository repository;
    private final EmployeeService employeeService;
    private final DocumentService documentService;
    private final CreateResultService<Employee> createResultService;
    
    @Autowired
    public EmployeeController(EmployeeRepository repository,
                              EmployeeService employeeService,
                              DocumentService documentService,
                              CreateResultService<Employee> createResultService) {
        this.repository = repository;
        this.employeeService = employeeService;
        this.documentService = documentService;
        this.createResultService = createResultService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Employee createOrUpdate(@RequestPart(value = "photo", required = false) MultipartFile photo,
                           @RequestPart("employee") String employeeJson) {
        return employeeService.saveEmployeeWithPhoto(employeeJson, photo);
    }

    @GetMapping("/{id}")
    public Optional<Employee> findById(@PathVariable String id) {
        return repository.findById(id);
    }

    @GetMapping
    public PageResultObject<Employee> findCurrentPage(@RequestParam("pageNum") int pageNum,
                                                    @RequestParam("pageSize") int pageSize,
                                                    @RequestParam(value = "nameFilter", defaultValue = "") String nameFilter) {
        PageRequest paging = PageRequest.of(pageNum, pageSize);
        Page<Employee> currentPage = repository.findByFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(nameFilter, nameFilter, paging);
        return createResultService.cretePagedResult(currentPage);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Employee update(@PathVariable String id, @RequestBody Employee employee){
        employee.setId(id);
        return repository.save(employee);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String id){
        documentService.removeDocumentsForEntity(id);
        repository.deleteById(id);
    }
}
