package com.documentmanagement.controller.result;

import com.documentmanagement.model.entity.Client;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.StreamSupport;

@Service
public class CreateResultService<T> {
    public PageResultObject<T> cretePagedResult(Page<T> currentPage){
        List<T> elementsForThisPage = StreamSupport.stream(currentPage.spliterator(), false).toList();
        return PageResultObject.<T>builder()
                .totalPages(currentPage.getTotalPages())
                .totalElements(currentPage.getTotalElements())
                .elements(elementsForThisPage)
                .build();
    }
}
