package com.documentmanagement.controller.result;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@Getter
@Setter
public class PageResultObject<T> {
    private long totalElements;
    private int totalPages;
    private List<T> elements;
}
