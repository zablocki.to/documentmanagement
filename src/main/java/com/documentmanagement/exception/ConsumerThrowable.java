package com.documentmanagement.exception;

import java.util.function.Consumer;

@FunctionalInterface
public interface ConsumerThrowable<T, E extends Exception> {
    void accept(T t) throws E;
}
