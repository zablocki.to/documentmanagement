package com.documentmanagement.exception;

import org.springframework.stereotype.Service;

import java.util.function.Consumer;
import java.util.function.Function;

public class ExceptionWrapper {
    public static <T, E extends Exception> Consumer<T> throwableWrapper(ConsumerThrowable<T, E> throwingConsumer) {
        return i -> {
            try {
                throwingConsumer.accept(i);
            } catch (Exception e){
                throw new RuntimeException(e);
            }
        };
    }
}
