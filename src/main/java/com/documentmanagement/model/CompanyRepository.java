package com.documentmanagement.model;

import com.documentmanagement.model.entity.Company;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepository extends ElasticsearchRepository<Company, String> {
}
