package com.documentmanagement.model;

import com.documentmanagement.model.entity.cons.AssignedEntity;
import com.documentmanagement.model.entity.DocumentEnt;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DocumentRepository extends ElasticsearchRepository<DocumentEnt, String> {
    List<DocumentEnt> findByAssignedEntityAndAssignedEntityId(AssignedEntity assignedEntity, String assignedEntityId);
    void deleteByAssignedEntityId(String assignedEntityId);
    Iterable<DocumentEnt> findAllByAssignedEntityId(String assignedEntityId);
}
