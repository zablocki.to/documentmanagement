package com.documentmanagement.model;

import com.documentmanagement.model.entity.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends ElasticsearchRepository<Employee, String> {
    @Query("{\"bool\": {\"should\": [{\"wildcard\": {\"firstName.keyword\": \"*?0*\"}}, {\"wildcard\": {\"lastName.keyword\": \"*?0*\"}}]}}")
    Page<Employee> findByFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(
            @Param("firstName") String firstName,
            @Param("lastName") String lastName,
            Pageable pageable);
}
