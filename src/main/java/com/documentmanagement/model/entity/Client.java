package com.documentmanagement.model.entity;

import com.documentmanagement.model.entity.cons.Gender;
import com.documentmanagement.model.entity.cons.GenderDeserializer;
import com.documentmanagement.model.entity.cons.GenderSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Getter
@Setter
@Document(indexName = "client")
public class Client {
    @Id
    private String id;
    @JsonDeserialize(using = GenderDeserializer.class)
    @JsonSerialize(using = GenderSerializer.class)
    private Gender gender;
    private String photoPath;
    private String firstName;
    private String lastName;
    private String streetAddress;
    private String postCodeAddress;
    private String cityAddress;
}
