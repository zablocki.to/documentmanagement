package com.documentmanagement.model.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Getter
@Setter
@Document(indexName = "company")
public class Company {
    @Id
    private String id;
    private String name;
    private String streetAddress;
    private String postCodeAddress;
    private String cityAddress;
}
