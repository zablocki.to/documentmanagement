package com.documentmanagement.model.entity;

import com.documentmanagement.model.entity.cons.AssignedEntity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Getter
@Setter
@Document(indexName = "document")
public class DocumentEnt {
    // id is made from: file name + type of entity (Client, company, employee) + entity id
    @Id
    private String id;
    private String name;
    private AssignedEntity assignedEntity;
    private String assignedEntityId;
    private String filePath;
}