package com.documentmanagement.model.entity.cons;

public enum AssignedEntity {
    CLIENT, COMPANY, EMPLOYEE;
}
