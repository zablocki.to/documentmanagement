package com.documentmanagement.model.entity.cons;

public enum Gender {
    MEN("M"), WOMEN("W");
    private final String label;

    Gender(String label) {
        this.label = label;
    }

    public static Gender fromValue(String label){
        for(Gender gender: Gender.values()){
            if (gender.label.equals(label)){
                return gender;
            }
        }
        throw new IllegalArgumentException(String.format("There are only gender values 'M' and 'W', not: %s", label));
    }

    public String getLabel() {
        return label;
    }
}