package com.documentmanagement.service;

import com.documentmanagement.model.ClientRepository;
import com.documentmanagement.model.EmployeeRepository;
import com.documentmanagement.model.entity.Client;
import com.documentmanagement.model.entity.Employee;
import com.documentmanagement.service.json.JsonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ClientService {
    private final ClientRepository repository;
    private final PhotoService photoService;

    @Autowired
    public ClientService(ClientRepository repository, PhotoService photoService) {
        this.repository = repository;
        this.photoService = photoService;
    }

    public Client saveClientWithPhoto(String clientJson, MultipartFile photo){
        Client client = JsonMapper.jsonToObject(clientJson, Client.class);
        photoService.savePhotoAndReturnItsPath(photo)
                .ifPresent(client::setPhotoPath);
        return repository.save(client);
    }

    public void removeAllClients() {
        repository.deleteAll();
    }
}
