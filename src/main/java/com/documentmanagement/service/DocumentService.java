package com.documentmanagement.service;

import com.documentmanagement.model.DocumentRepository;
import com.documentmanagement.model.entity.DocumentEnt;
import com.documentmanagement.service.json.JsonMapper;
import com.documentmanagement.service.storage.FileStorageService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Service
@Slf4j
public class DocumentService {
    @Value("${storage.bean.name}")
    private String storageBeanName;

    private final DocumentRepository documentRepository;

    private final ApplicationContext applicationContext;
    private FileStorageService fileStorage;

    @Autowired
    public DocumentService(DocumentRepository documentRepository, ApplicationContext applicationContext) {
        this.documentRepository = documentRepository;
        this.applicationContext = applicationContext;
    }

    @PostConstruct
    @Profile("prod")
    void injectChosenStorageService(){
        this.fileStorage = (FileStorageService) applicationContext.getBean(storageBeanName);
    }

    @PostConstruct
    @Profile("dev")
    void injectLocalStorageService(){
        this.fileStorage = (FileStorageService) applicationContext.getBean("localStorageService");
    }

    public String saveFile(MultipartFile file) throws IOException {
        return fileStorage.writeFile(file);
    }

    public DocumentEnt saveDocumentMetadata(String documentJson, String filePath) throws IOException {
        DocumentEnt document = JsonMapper.jsonToObject(documentJson, DocumentEnt.class);
        document.setFilePath(filePath);
        return documentRepository.save(document);
    }

    public Optional<DocumentEnt> removeFile(DocumentEnt doc) {
        boolean ifRemoved = fileStorage.removeFile(doc.getFilePath());
        return ifRemoved ? Optional.of(doc) : Optional.empty();
    }

    public DocumentEnt removeDocumentMetadata(DocumentEnt doc) {
        documentRepository.delete(doc);
        return doc;
    }

    public void removeDocumentsForEntity(String entityId) {
        Iterable<DocumentEnt> allDocumentsToDelete = documentRepository.findAllByAssignedEntityId(entityId);
        StreamSupport.stream(allDocumentsToDelete.spliterator(), false)
                .forEach(d -> fileStorage.removeFile(d.getFilePath()));
        documentRepository.deleteByAssignedEntityId(entityId);
    }

    public void removeAllDocuments() {
        List<String> removedFiles = fileStorage.removeAllFiles();
        removedFiles.forEach(f -> log.info(String.format("File with name: %s removed.", f)));
        documentRepository.deleteAll();
    }
}
