package com.documentmanagement.service;

import com.documentmanagement.model.EmployeeRepository;
import com.documentmanagement.model.entity.Employee;
import com.documentmanagement.service.json.JsonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class EmployeeService {
    private final EmployeeRepository repository;
    private final PhotoService photoService;

    @Autowired
    public EmployeeService(EmployeeRepository repository, PhotoService photoService) {
        this.repository = repository;
        this.photoService = photoService;
    }

    public Employee saveEmployeeWithPhoto(String employeeJson, MultipartFile photo){
        Employee employee = JsonMapper.jsonToObject(employeeJson, Employee.class);
        photoService.savePhotoAndReturnItsPath(photo)
                .ifPresent(employee::setPhotoPath);
        return repository.save(employee);
    }

    public void removeAllEmployees() {
        repository.deleteAll();
    }
}
