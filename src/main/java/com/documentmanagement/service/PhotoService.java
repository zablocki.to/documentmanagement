package com.documentmanagement.service;

import com.documentmanagement.service.storage.FileStorageService;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

@Service
public class PhotoService {
    @Value("${storage.bean.name}")
    private String storageBeanName;
    private FileStorageService fileStorage;
    private ApplicationContext applicationContext;

    @Autowired
    public PhotoService(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @PostConstruct
    @Profile("prod")
    void injectChosenStorageService(){
        this.fileStorage = (FileStorageService) applicationContext.getBean(storageBeanName);
    }

    @PostConstruct
    @Profile("dev")
    void injectLocalStorageService(){
        this.fileStorage = (FileStorageService) applicationContext.getBean("localStorageService");
    }

    public Optional<String> savePhotoAndReturnItsPath(MultipartFile photo){
        return Optional.ofNullable(photo)
                .map(p -> this.fileStorage.writeFile(p));
    }
}
