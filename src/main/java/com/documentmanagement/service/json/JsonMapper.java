package com.documentmanagement.service.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JsonMapper {
    public static <T> T jsonToObject(String json, Class<T> clazz){
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(json, clazz);
        } catch (JsonProcessingException e) {
            log.error(String.format("Json: %s can't be casted into class: %s.", json, clazz));
            throw new RuntimeException(e);
        }
    }
}
