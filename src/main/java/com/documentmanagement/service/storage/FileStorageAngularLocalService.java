package com.documentmanagement.service.storage;

import com.documentmanagement.exception.ConsumerThrowable;
import com.documentmanagement.exception.ExceptionWrapper;
import com.documentmanagement.service.storage.filemodify.FileNameUniquer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.documentmanagement.exception.ExceptionWrapper.throwableWrapper;
import static java.util.stream.Collectors.toList;

@Slf4j
@Service("localStorageService")
@Profile({"dev", "prod"})
public class FileStorageAngularLocalService implements FileStorageService{
    @Value("${local.files.path}")
    private String localStoragePath;
    private static final String localStorageUrl = "http://localhost:8080/local/files/";

    @Override
    public String writeFile(MultipartFile file) {
        try {
            Files.createDirectories(Paths.get(localStoragePath));
            String fileName = FileNameUniquer.addHashToTheFile(file.getOriginalFilename());
            String localFilePath = localStoragePath + fileName;
            File serverFile = new File(localFilePath);
            file.transferTo(serverFile);
            return localStorageUrl + fileName;
        } catch (IOException e) {
           throw new RuntimeException(e);
        }
    }

    @Override
    public boolean removeFile(String path) {
        String fileName = path.replace(localStorageUrl, "");
        String localFilePath = localStoragePath + fileName;
        try {
            return Files.deleteIfExists(Paths.get(localFilePath));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<String> removeAllFiles() {
        try (Stream<Path> storedFiles = Files.list(Paths.get(localStoragePath))){
            List<Path> storedFilePaths = storedFiles.toList();
            storedFilePaths.forEach(throwableWrapper(Files::deleteIfExists));
            return storedFilePaths.stream()
                    .map(p -> p.getFileName().toString())
                    .collect(toList());
        } catch (IOException e) {
            log.error(String.format("Problem with removing a files: %s", e.getMessage()));
            throw new RuntimeException(e);
        }
    }
}
