package com.documentmanagement.service.storage;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface FileStorageService {
    String writeFile(MultipartFile file);
    boolean removeFile(String path);
    List<String> removeAllFiles();
}
