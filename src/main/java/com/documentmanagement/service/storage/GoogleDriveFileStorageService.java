package com.documentmanagement.service.storage;


import com.documentmanagement.service.storage.filemodify.FileNameUniquer;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.Permission;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;

@Service("googleDriveStorageService")
@Profile("prod")
public class GoogleDriveFileStorageService implements FileStorageService{

    @Value("${google.drive.certificate.path}")
    private String credentialPath;

    @Value("${google.drive.application.name}")
    private String googleDriveAppName;

    @Value("${google.drive.tokens.directory.name}")
    private String tokenDirName;

    private static final JsonFactory jsonFactory = GsonFactory.getDefaultInstance();

    @Override
    public String writeFile(MultipartFile fileToUpload) {
        try {
            final NetHttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            Drive service = new Drive.Builder(httpTransport, jsonFactory, getCredentials(httpTransport))
                    .setApplicationName(googleDriveAppName)
                    .build();

            //java.io.File fileToUpload = new java.io.File("/Users/tomaszzablocki/Documents/projects/DMS/DocumentManagement/src/main/resources/storageFortest/test.txt");

            File fileMetadata = new File();
            fileMetadata.setName(FileNameUniquer.addHashToTheFile(fileToUpload.getOriginalFilename()));
            java.io.File tempFileToUpload = java.io.File.createTempFile("prefix-", "-suffix");
            fileToUpload.transferTo(tempFileToUpload);
            FileContent mediaContent = new FileContent(fileToUpload.getContentType(), tempFileToUpload); //"text/plain"

            File uploadedFile = service.files().create(fileMetadata, mediaContent)
                    .setFields("id, webContentLink")
                    .execute();

            makeUploadedFilePublic(service, uploadedFile);

            return uploadedFile.getWebContentLink();
        } catch (GeneralSecurityException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean removeFile(String path) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @Override
    public List<String> removeAllFiles() {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    private Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT)
            throws IOException {
        InputStream in = GoogleDriveFileStorageService.class.getResourceAsStream(credentialPath);
        if (in == null) {
            throw new FileNotFoundException("Resource not found: " + credentialPath);
        }
        GoogleClientSecrets clientSecrets =
                GoogleClientSecrets.load(jsonFactory, new InputStreamReader(in));

        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, jsonFactory, clientSecrets, Collections.singletonList(DriveScopes.DRIVE))
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(tokenDirName)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }

    private static void makeUploadedFilePublic(Drive service, File uploadedFile) throws IOException {
        Permission permission = new Permission()
                .setType("anyone")
                .setRole("reader");

        service.permissions().create(uploadedFile.getId(), permission).execute();
    }
}
