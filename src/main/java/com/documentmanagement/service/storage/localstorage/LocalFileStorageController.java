package com.documentmanagement.service.storage.localstorage;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Profile("dev")
@RestController
@RequestMapping("local/files")
public class LocalFileStorageController {
    @Value("${local.files.path}")
    private String localFilesPath;

    @GetMapping("/{fileName}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName) throws MalformedURLException {
        Path filePath = Paths.get(localFilesPath).resolve(fileName);
        if (Files.notExists(filePath)){
            return ResponseEntity.notFound().build();
        }
        UrlResource localFile = new UrlResource(filePath.toUri());
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename="+fileName);
        return ResponseEntity.ok()
                             .headers(headers)
                             .body(localFile);
    }
}
