package com.documentmanagement.controller;

import com.documentmanagement.elasticsearch.ElasticSearchContainerImpl;
import com.documentmanagement.model.entity.Client;
import com.documentmanagement.model.entity.cons.Gender;
import com.documentmanagement.controller.result.PageResultObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.IndexOperations;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.mock.web.MockPart;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Testcontainers
@ActiveProfiles({"test", "dev"})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test.properties")
@AutoConfigureMockMvc
class ClientControllerIT {
    @Container
    private static final ElasticSearchContainerImpl elasticsearchContainer = new ElasticSearchContainerImpl();
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;
    @Autowired
    private MockMvc mvc;

    @BeforeAll
    public static void setUp() {
        elasticsearchContainer.start();
        assertThat(elasticsearchContainer.isRunning()).isTrue();
    }

    @AfterAll
    static void tearDown() {
        elasticsearchContainer.stop();
    }

    @BeforeEach
    void resetIndex(){
        IndexOperations clientIndex = elasticsearchTemplate.indexOps(Client.class);
        if (clientIndex.exists()){
            clientIndex.delete();
            clientIndex.create();
        }
    }

    @Test
    void createNewClientWithPhotoShouldAddClientIntoElasticSearchWithPhotoPath() throws Exception {
        MvcResult mvcResult = mvc.perform(
                multipart(HttpMethod.POST, "/clients")
                        .file(createMultipartPhoto())
                        .part(createMultipartClient()))
                .andExpect(status().isCreated())
                .andReturn();

        Client result = getResultFromMvc(mvcResult);
        Client clientFromElasticSearch = getSavedClientFromElasticSearch(result);

        assertThat(result.getId()).isNotNull();
        assertThat(result.getPhotoPath()).isNotEmpty();
        assertThat(result.getGender()).isEqualTo(clientFromElasticSearch.getGender());
        assertThat(result.getPhotoPath()).isEqualTo(clientFromElasticSearch.getPhotoPath());
        assertThat(result.getFirstName()).isEqualTo(clientFromElasticSearch.getFirstName());
        assertThat(result.getLastName()).isEqualTo(clientFromElasticSearch.getLastName());
        assertThat(result.getStreetAddress()).isEqualTo(clientFromElasticSearch.getStreetAddress());
        assertThat(result.getPostCodeAddress()).isEqualTo(clientFromElasticSearch.getPostCodeAddress());
        assertThat(result.getCityAddress()).isEqualTo(clientFromElasticSearch.getCityAddress());
    }

    @Test
    void createNewClientWithoutPhotoShouldAddClientIntoElasticSearchWithEmptyPhotoPath() throws Exception {
        MvcResult mvcResult = mvc.perform(
                        multipart(HttpMethod.POST, "/clients")
                                .part(createMultipartClient()))
                .andExpect(status().isCreated())
                .andReturn();

        Client result = getResultFromMvc(mvcResult);
        Client clientFromElasticSearch = getSavedClientFromElasticSearch(result);

        assertThat(result.getId()).isNotNull();
        assertThat(result.getPhotoPath()).isNull();
        assertThat(result.getGender()).isEqualTo(clientFromElasticSearch.getGender());
        assertThat(result.getFirstName()).isEqualTo(clientFromElasticSearch.getFirstName());
        assertThat(result.getLastName()).isEqualTo(clientFromElasticSearch.getLastName());
        assertThat(result.getStreetAddress()).isEqualTo(clientFromElasticSearch.getStreetAddress());
        assertThat(result.getPostCodeAddress()).isEqualTo(clientFromElasticSearch.getPostCodeAddress());
        assertThat(result.getCityAddress()).isEqualTo(clientFromElasticSearch.getCityAddress());
    }

    @Test
    void createNewClientWithoutProvidingClientInPostShouldFail() throws Exception {
        mvc.perform(
                        multipart(HttpMethod.POST, "/clients")
                                .file(createMultipartPhoto()))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void findByIdShouldReturnClientFromElasticSearchIfItIsPresent() throws Exception {
        Client randomClient = createClient(3);
        elasticsearchTemplate.save(
                createClient(1),
                createClient(2),
                randomClient,
                createClient(4)
                );
        refreshIndex();

        MvcResult mvcResult = mvc.perform(get("/clients/" + randomClient.getId()))
                .andExpect(status().isOk())
                .andReturn();

        Client result = getResultFromMvc(mvcResult);

        assertThat(result.getGender()).isEqualTo(randomClient.getGender());
        assertThat(result.getPhotoPath()).isEqualTo(randomClient.getPhotoPath());
        assertThat(result.getFirstName()).isEqualTo(randomClient.getFirstName());
        assertThat(result.getLastName()).isEqualTo(randomClient.getLastName());
        assertThat(result.getStreetAddress()).isEqualTo(randomClient.getStreetAddress());
        assertThat(result.getPostCodeAddress()).isEqualTo(randomClient.getPostCodeAddress());
        assertThat(result.getCityAddress()).isEqualTo(randomClient.getCityAddress());
    }

    @Test
    void findByIdShouldReturnEmptyContentFromElasticSearchIfItIsNotPresent() throws Exception {
        Client randomClient = createClient(3);
        elasticsearchTemplate.save(
                createClient(1),
                createClient(2),
                createClient(4)
        );
        refreshIndex();

        MvcResult mvcResult = mvc.perform(get("/clients/" + randomClient.getId()))
                .andExpect(status().isOk())
                .andReturn();

        String result = mvcResult.getResponse().getContentAsString();

        assertThat(result).isEqualTo("null");
    }

    @Test
    void findCurrentPageShouldReturnFirstPageWithTwoRecordsIfPageNumSetTo1AndPageSizeTo2() throws Exception {
        Client randomClient1 = createClient(3);
        Client randomClient2 = createClient(4);
        elasticsearchTemplate.save(
                createClient(1),
                createClient(2),
                randomClient1,
                randomClient2,
                createClient(5)
        );
        refreshIndex();

        MvcResult mvcResult = mvc.perform(get("/clients?pageNum=1&pageSize=2"))
                .andExpect(status().isOk())
                .andReturn();

        String result = mvcResult.getResponse().getContentAsString();

        assertThat(result).isEqualTo(String.format("{\"totalElements\":5,\"totalPages\":3,\"elements\":[{\"id\":\"%s\",\"gender\":\"M\",\"photoPath\":\"/images/clients/123456_3.jpg\",\"firstName\":\"John3\",\"lastName\":\"Doe3\",\"streetAddress\":\"3 Main St\",\"postCodeAddress\":\"12343\",\"cityAddress\":\"Springfield\"},{\"id\":\"%s\",\"gender\":\"M\",\"photoPath\":\"/images/clients/123456_4.jpg\",\"firstName\":\"John4\",\"lastName\":\"Doe4\",\"streetAddress\":\"4 Main St\",\"postCodeAddress\":\"12344\",\"cityAddress\":\"Springfield\"}]}", randomClient1.getId(), randomClient2.getId()));
    }

    @Test
    void findCurrentPageShouldReturnThirdPageWithOneRecordsIfPageNumSetTo3AndPageSizeTo2() throws Exception {
        Client randomClient = createClient(5);
        elasticsearchTemplate.save(
                createClient(1),
                createClient(2),
                createClient(3),
                createClient(4),
                randomClient
        );
        refreshIndex();

        MvcResult mvcResult = mvc.perform(get("/clients?pageNum=2&pageSize=2"))
                .andExpect(status().isOk())
                .andReturn();

        String result = mvcResult.getResponse().getContentAsString();

        assertThat(result).isEqualTo(String.format("{\"totalElements\":5,\"totalPages\":3,\"elements\":[{\"id\":\"%s\",\"gender\":\"M\",\"photoPath\":\"/images/clients/123456_5.jpg\",\"firstName\":\"John5\",\"lastName\":\"Doe5\",\"streetAddress\":\"5 Main St\",\"postCodeAddress\":\"12345\",\"cityAddress\":\"Springfield\"}]}", randomClient.getId()));
    }

    @Test
    void findCurrentPageShouldReturnFirstPageWithOneRecordsIfPageNumSetTo1AndPageSizeTo2AndNameFilterTo1() throws Exception {
        Client elementToFilter = createClient(1);
        elasticsearchTemplate.save(
                elementToFilter,
                createClient(2),
                createClient(3),
                createClient(4),
                createClient(5)
        );
        refreshIndex();

        MvcResult mvcResult = mvc.perform(get("/clients?pageNum=0&pageSize=2&nameFilter=1"))
                .andExpect(status().isOk())
                .andReturn();

        String result = mvcResult.getResponse().getContentAsString();

        assertThat(result).isEqualTo(String.format("{\"totalElements\":1,\"totalPages\":1,\"elements\":[{\"id\":\"%s\",\"gender\":\"M\",\"photoPath\":\"/images/clients/123456_1.jpg\",\"firstName\":\"John1\",\"lastName\":\"Doe1\",\"streetAddress\":\"1 Main St\",\"postCodeAddress\":\"12341\",\"cityAddress\":\"Springfield\"}]}", elementToFilter.getId()));
    }

    @Test
    void findCurrentPageShouldReturnFirstPageWithNoRecordsIfPageNumSetTo1AndPageSizeTo2AndNameFilterToNotExistingMarian() throws Exception {
        elasticsearchTemplate.save(
                createClient(1),
                createClient(2),
                createClient(3),
                createClient(4),
                createClient(5)
        );
        refreshIndex();

        MvcResult mvcResult = mvc.perform(get("/clients?pageNum=3&pageSize=2&nameFilter=Marian"))
                .andExpect(status().isOk())
                .andReturn();

        String result = mvcResult.getResponse().getContentAsString();

        assertThat(result).isEqualTo("{\"totalElements\":0,\"totalPages\":0,\"elements\":[]}");
        /*
        assertThat(result.getElements().size()).isEqualTo(0);
        assertThat(result.getTotalPages()).isEqualTo(1);
        assertThat(result.getTotalElements()).isEqualTo(0);

         */
    }

    @Test
    void updateShouldChangeAlreadyExistingClientRecord() throws Exception {
        Client randomClient = createClient(3);
        elasticsearchTemplate.save(
                createClient(1),
                createClient(2),
                randomClient,
                createClient(4)
        );
        refreshIndex();

        String clientToUpdateId = randomClient.getId();
        Client updateClientData = createClient(5);
        mvc.perform(
                        put("/clients/" + clientToUpdateId)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(new ObjectMapper().writeValueAsString(updateClientData)))
                .andExpect(status().isCreated())
                .andReturn();
        Client updatedClient = elasticsearchTemplate.get(clientToUpdateId, Client.class);

        assertThat(updatedClient).isNotNull();
        assertThat(updatedClient.getGender()).isEqualTo(updateClientData.getGender());
        assertThat(updatedClient.getPhotoPath()).isEqualTo(updateClientData.getPhotoPath());
        assertThat(updatedClient.getFirstName()).isEqualTo(updateClientData.getFirstName());
        assertThat(updatedClient.getLastName()).isEqualTo(updateClientData.getLastName());
        assertThat(updatedClient.getStreetAddress()).isEqualTo(updateClientData.getStreetAddress());
        assertThat(updatedClient.getPostCodeAddress()).isEqualTo(updateClientData.getPostCodeAddress());
        assertThat(updatedClient.getCityAddress()).isEqualTo(updateClientData.getCityAddress());
    }

    @Test
    void deleteShouldRemoveClientIfExist() throws Exception {
        Client randomClient = createClient(3);
        elasticsearchTemplate.save(
                createClient(1),
                createClient(2),
                randomClient,
                createClient(4)
        );
        refreshIndex();

        String idOfClientToDelete = randomClient.getId();
        mvc.perform(delete("/clients/"+idOfClientToDelete))
                .andExpect(status().isNoContent());

        Client deletedClient = elasticsearchTemplate.get(idOfClientToDelete, Client.class);

        assertThat(deletedClient).isNull();
    }

    @Test
    void deleteShouldNotFailIfClientNotExist() throws Exception {
        Client randomClient = createClient(3);
        elasticsearchTemplate.save(
                createClient(1),
                createClient(2),
                createClient(4)
        );
        refreshIndex();

        String idOfClientToDelete = randomClient.getId();
        mvc.perform(delete("/clients/"+idOfClientToDelete))
                .andExpect(status().isNoContent());

        Client deletedClient = elasticsearchTemplate.get(idOfClientToDelete, Client.class);

        assertThat(deletedClient).isNull();
    }

    private MockMultipartFile createMultipartPhoto() throws IOException {
        return new MockMultipartFile(
                "photo",
                "test_photo.png",
                MediaType.IMAGE_PNG_VALUE,
                Files.readAllBytes(Paths.get(System.getProperty("user.dir") +"/src/test/integration/resources/test_photo.png"))
        );
    }

    private MockPart createMultipartClient(){
        String clientJson = "{"
                + "\"gender\":\"M\","
                + "\"firstName\":\"John\","
                + "\"lastName\":\"Doe\","
                + "\"streetAddress\":\"123 Main St\","
                + "\"postCodeAddress\":\"12345\","
                + "\"cityAddress\":\"Springfield\""
                + "}";
        return new MockPart("client",
                clientJson.getBytes());
    }

    private Client getResultFromMvc(MvcResult mvcResult) throws JsonProcessingException, UnsupportedEncodingException {
        String resultContent = mvcResult.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new Jdk8Module());
        return mapper.readValue(resultContent, Client.class);//JsonMapper.jsonToObject(resultContent, clazz);
    }

    private Client getSavedClientFromElasticSearch(Client savedClient){
        String createdClientId = savedClient.getId();
        return elasticsearchTemplate.get(createdClientId, Client.class);
    }

    private Client createClient(int num){
        Client client = new Client();
        client.setId(UUID.randomUUID().toString());
        client.setGender(Gender.MEN);
        client.setPhotoPath(String.format("/images/clients/123456_%d.jpg", num));
        client.setFirstName("John"+num);
        client.setLastName("Doe"+num);
        client.setStreetAddress(String.format("%d Main St", num));
        client.setPostCodeAddress("1234"+num);
        client.setCityAddress("Springfield");
        return client;
    }

    /**
     * Refresh Index is needed as operations by default are asynchronous and next one can be quicker than adding data.
     */
    private void refreshIndex(){
        elasticsearchTemplate.indexOps(Client.class).refresh();
    }
}