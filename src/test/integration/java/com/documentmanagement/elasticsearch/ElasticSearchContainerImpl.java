package com.documentmanagement.elasticsearch;

import org.testcontainers.elasticsearch.ElasticsearchContainer;
import org.testcontainers.utility.DockerImageName;

public class ElasticSearchContainerImpl extends ElasticsearchContainer {
    private static final String ELASTICSEARCH_DOCKER_IMAGE = "elasticsearch:8.10.2";
    private static final String CLUSTER_NAME = "cluster.name";
    private static final String ELASTICSEARCH = "elasticsearch";
    private static final String DISCOVERY_TYPE = "discovery.type";
    private static final String SINGLE_NODE = "single-node";
    private static final String XPACK_SECURITY_ENABLED = "xpack.security.enabled";
    private static final String ELASTIC_USERNAME = "ELASTIC_USERNAME";
    private static final String USER_NAME = "testusername";
    private static final String ELASTIC_PASSWORD = "ELASTIC_PASSWORD";
    private static final String PASSWORD = "testpassword";

    public ElasticSearchContainerImpl() {
        super(DockerImageName.parse(ELASTICSEARCH_DOCKER_IMAGE)
                .asCompatibleSubstituteFor("docker.elastic.co/elasticsearch/elasticsearch"));
        addFixedExposedPort(9200, 9200);
        addEnv(DISCOVERY_TYPE, SINGLE_NODE);
        addEnv(XPACK_SECURITY_ENABLED, Boolean.FALSE.toString());
        addEnv(CLUSTER_NAME, ELASTICSEARCH);
        addEnv(ELASTIC_USERNAME, USER_NAME);
        addEnv(ELASTIC_PASSWORD, PASSWORD);
    }
}
