package com.documentmanagement.controller.result;

import com.documentmanagement.model.entity.Employee;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class CreateResultServiceTest {
    private CreateResultService<TestEntity> out = new CreateResultService<>();

    @Test
    void cretePagedResultShouldCreateWrapperObjectWith5ElementsIfPagedResultWith5ElementsProvided() {
        Page<TestEntity> pagesDbResult = new PageImpl<>(List.of(
                new TestEntity(1, "a"),
                new TestEntity(2, "b"),
                new TestEntity(3, "c"),
                new TestEntity(4, "d"),
                new TestEntity(5, "e")
        ), Pageable.ofSize(2), 5);

        PageResultObject<TestEntity> result = out.cretePagedResult(pagesDbResult);

        assertThat(result.getTotalElements()).isEqualTo(5);
        assertThat(result.getTotalPages()).isEqualTo(3);
        assertThat(result.getElements()).hasSize(5);
    }

    @Test
    void cretePagedResultShouldReturnEmptyWrapperObjectIfNoPagedResultsProvided() {
        Page<TestEntity> pagesDbResult = Page.empty();

        PageResultObject<TestEntity> result = out.cretePagedResult(pagesDbResult);

        assertThat(result.getTotalElements()).isEqualTo(0);
        assertThat(result.getTotalPages()).isEqualTo(1);
        assertThat(result.getElements()).hasSize(0);
    }

    @Getter
    @Setter
    @AllArgsConstructor
    static class TestEntity{
        int id;
        String name;
    }
}