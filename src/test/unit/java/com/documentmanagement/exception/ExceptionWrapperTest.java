package com.documentmanagement.exception;

import org.junit.jupiter.api.Test;

import static com.documentmanagement.exception.ExceptionWrapper.throwableWrapper;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class ExceptionWrapperTest {

    @Test
    void throwableWrapperShouldWrapAnyExceptionThrownByConsumerIntoRuntimeException() {
        String message = "Test message";

        try {
          throwableWrapper(createExceptionThrowingConsumer(message));
        } catch (RuntimeException e){
            assertThat(e.getMessage()).isEqualTo(message);
        }
    }

    private ConsumerThrowable<Integer, Exception> createExceptionThrowingConsumer(String message){
        return i -> {throw new Exception(message);};
    }
}