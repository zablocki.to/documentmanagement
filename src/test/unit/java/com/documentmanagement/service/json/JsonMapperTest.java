package com.documentmanagement.service.json;

import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class JsonMapperTest {
    @Test
    void jsonToObjectShouldParseCorrectJsonString() {
        TestObject result = JsonMapper.jsonToObject("{\"a\":\"some\", \"b\":10}", TestObject.class);
        assertThat(result.a).isEqualTo("some");
        assertThat(result.b).isEqualTo(10);
    }

    @Test
    void jsonToObjectShouldFailOnIncorrectJsonString() {
        try {
            JsonMapper.jsonToObject("{\"aa\":\"some\", \"bb\":10}", TestObject.class);
        } catch (RuntimeException e){
            assertThat(e.getMessage()).contains("com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException");
        }
    }

    @Getter
    @Setter
    static class TestObject{
        private String a;
        private int b;
    }
}