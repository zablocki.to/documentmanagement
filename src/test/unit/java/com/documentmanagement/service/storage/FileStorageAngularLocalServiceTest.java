package com.documentmanagement.service.storage;

import org.junit.jupiter.api.*;
import org.mockito.Mock;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.IntStream;

import static com.documentmanagement.exception.ExceptionWrapper.throwableWrapper;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class FileStorageAngularLocalServiceTest {
    private static final FileStorageAngularLocalService out = new FileStorageAngularLocalService();
    private static final String LOCAL_FILES_PATH = System.getProperty("user.dir") + "/src/test/java/com/documentmanagement/service/storage/storageForTest/";

    @BeforeEach
    void prepareTests() throws IOException {
        ReflectionTestUtils.setField(out, "localStoragePath", LOCAL_FILES_PATH);
        Files.createDirectory(Paths.get(LOCAL_FILES_PATH));
    }

    @AfterEach
    void cleanAfterTests() throws IOException {
        Files.list(Paths.get(LOCAL_FILES_PATH)).forEach(throwableWrapper(Files::deleteIfExists));
        Files.deleteIfExists(Paths.get(LOCAL_FILES_PATH));
    }

    @Test
    void writeFileShouldSerializeFileOnLocalDiscForMultipartFileProvided() throws IOException {
        final String originalFileName = "example_file.txt";
        final String exampleContent = "Example content";
        MultipartFile testFile = new MockMultipartFile("name", originalFileName, "text/plain", exampleContent.getBytes(StandardCharsets.UTF_8));
        String result = out.writeFile(testFile);

        String generatedFileUrl = "http://localhost:8080/local/files/";
        String generatedFileName = result.replace(generatedFileUrl, "");
        assertThat(result).startsWith(generatedFileUrl);
        assertThat(result).endsWith(originalFileName);
        assertThat(Files.exists(Paths.get(LOCAL_FILES_PATH))).isTrue();
        assertThat(Files.readString(Paths.get(LOCAL_FILES_PATH+generatedFileName))).isEqualTo(exampleContent);
    }

    @Test
    void removeFileShouldRemoveExistingFile() throws IOException {
        final String fileName = "file_to_remove.txt";
        final Path fileToRemovePath = Paths.get(LOCAL_FILES_PATH + fileName);
        final String fileUrl = "http://localhost:8080/local/files/" + fileName;
        Files.createFile(fileToRemovePath);

        boolean ifRemoved = out.removeFile(fileUrl);

        assertThat(ifRemoved).isTrue();
        assertThat(Files.exists(fileToRemovePath)).isFalse();
    }

    @Test
    void removeFileShouldNotRemoveNotExistingFile() throws IOException {
        final String notExistingFileUrl = "http://localhost:8080/local/files/file_to_remove.txt";

        boolean ifRemoved = out.removeFile(notExistingFileUrl);

        assertThat(ifRemoved).isFalse();
    }

    @Test
    void removeAllFiles() {
        final String basicFileName = "fileName%d.txt";
        IntStream.range(0,5).boxed()
                .map(i -> String.format(basicFileName, i))
                .forEach(throwableWrapper(fn -> Files.createFile(Paths.get(LOCAL_FILES_PATH+fn))));

        List<String> removedFileNames = out.removeAllFiles();

        assertThat(removedFileNames).containsAll(
                List.of("fileName0.txt",
                        "fileName1.txt",
                        "fileName2.txt",
                        "fileName3.txt",
                        "fileName4.txt")
        );
    }
}