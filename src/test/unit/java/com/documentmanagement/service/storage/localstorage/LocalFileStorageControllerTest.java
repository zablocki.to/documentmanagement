package com.documentmanagement.service.storage.localstorage;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.as;
import static org.assertj.core.api.Assertions.assertThat;

class LocalFileStorageControllerTest {
    private static final LocalFileStorageController out = new LocalFileStorageController();
    private static final String LOCAL_FILES_PATH = System.getProperty("user.dir") + "/src/test/java/com/documentmanagement/service/storage/localstorage/storageForTest/";
    private static final String TEST_FILE_CONTENT = "Example Content";
    private static final String TEST_FILE_NAME = "example_file.txt";

    @BeforeAll
    static void prepareTestEnv() throws IOException {
        ReflectionTestUtils.setField(out, "localFilesPath", LOCAL_FILES_PATH);
        Files.createDirectory(Paths.get(LOCAL_FILES_PATH));
        Files.writeString(Paths.get(LOCAL_FILES_PATH + TEST_FILE_NAME), TEST_FILE_CONTENT);
    }

    @AfterAll
    static void cleanAfterTests() throws IOException {
        Files.deleteIfExists(Paths.get(LOCAL_FILES_PATH + TEST_FILE_NAME));
        Files.deleteIfExists(Paths.get(LOCAL_FILES_PATH));
    }

    @Test
    void downloadFileShouldReturnResponseWithExistingFile() throws MalformedURLException {
        ResponseEntity<Resource> response = out.downloadFile(TEST_FILE_NAME);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.hasBody()).isTrue();
    }

    @Test
    void downloadFileShouldFailForNotExistingFile() throws MalformedURLException {
        String notExistingFileName = "not_existing.txt";
        ResponseEntity<Resource> response = out.downloadFile(notExistingFileName);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.hasBody()).isFalse();
    }
}